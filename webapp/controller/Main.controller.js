sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/base/Log",
	"sap/ui/model/json/JSONModel"
],
	/**
	 * @param {typeof sap.ui.core.mvc.Controller} Controller
	 */
	function (Controller, Log, JSONModel) {
		"use strict";
		
		return Controller.extend("com.hex.student.controller.Main", {
			/** lifecysle method
			 * @constructor onInit is the  lifecysle function name
			 */
			onInit: function () {
			
				this.oEmployeeModel = new JSONModel(
					{
						"EmployeeSet": null
					}

				);
				this.getView().setModel(this.oEmployeeModel, "employee");
				this.getView().byId("id_table_disorder").setVisible(false);

			},
			onPressButton: function () {
				sap.m.MessageToast.show("Button Pressed");
				this._callEmployeeService()
			},
			_callEmployeeService: function () {
				var oGlobalBusyDialog = new sap.m.BusyDialog();
				oGlobalBusyDialog.open();
var 	 that = this;
				let oDataModel = this.getView().getModel();
				oDataModel.read("/empregistrationSet", {

					success: function (oResults, oResponse) {
						if (oResponse.statusCode === "200" || 200) {
							var aResults = Object.keys(oResults)
								.map(function (key) {
									return [Number(key), oResults[key]];
								});
							var aEmployeeSet = aResults[0][1]; //getting the collection into array object.
							if (aEmployeeSet.length != 0) {
								oGlobalBusyDialog.close();
								that.setOdatatoJSONMdoel(aEmployeeSet);
							}
							Log.info("Successfully Consued" + aEmployeeSet);
						} else {
							Log.error("Error in the retrival of the CollectionSet");
						}
					},
					error: function () {
						oGlobalBusyDialog.close();
						that.getView().byId("id_table_disorder").setVisible(false);
						Log.error("Error in the OData Service consumption to read");
					}
				});
			},
			setOdatatoJSONMdoel: function (aEmployeeSet) {
			
				this.getView().getModel("employee").setProperty("/EmployeeSet", aEmployeeSet)
				this.getView().byId("id_table_disorder").setVisible(true);
			}
		});
	});
